<?php

namespace App\Http\Controllers;


class ExerciseController extends Controller {

    public function resolve(){

        $results = array();

        $result['fizzbuzz'] = $this->fizzBuzz();
        $result['fibonacci1'] = $this->fibonacci(1);
        $result['fibonacci5'] = $this->fibonacci(5);
        $result['fibonacci10'] = $this->fibonacci(10);

        return view('viewResults', ['result' => $result]);

    }

    /**
     * @param int $int
     * @return bool|string
     */
    private function fizzBuzz( int $int = 20 ) {

        $str = "";

        for ($i = 1; $i <= $int ; $i++) {

            if ( $i%3 == 0 && $i%5 == 0 ) {

                    $str .= '"fizzbuzz" ,';
            }
            else if ( $i%3 == 0 ) {

                $str .= '"fizz", ';
            }
            else if ( $i%5 == 0 ) {

                $str .= '"buzz", ';
            }
            else {
                $str .= $i.", ";
            }
        }

        return substr($str, 0, -2);

    }

    /**
     * @param int $n
     * @return string
     */
    public function fibonacci( int $n ){

        $number1 = 0;
        $number2 = 1;

        $result = '';
        $counter = 0;

        while ($counter < $n){
            $result .= ' '.$number1;
            $num3 = $number2 + $number1;
            $number1 = $number2;
            $number2 = $num3;
            $counter = $counter + 1;
        }

        return $result;
    }





}
