<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Paylink Excercise</title>
</head>
<body>

<div class="tab-pane">
    <h1>Fizzbuzz</h1>
    <p><?php echo $result['fizzbuzz'];?></p>
</div>
<div class="tab-pane">
    <h1>Fibonacci</h1>
    <p>fibonacci(1) =  <?php echo $result['fibonacci1'];?></p>
    <p>fibonacci(5) =  <?php echo $result['fibonacci5'];?></p>
    <p>fibonacci(10) =  <?php echo $result['fibonacci10'];?></p>
</div>
</body>
</html>